package no.uib.inf101.gridview;

import java.awt.Color;
import javax.swing.JFrame;

import no.uib.inf101.datastructure.CellPosition;
import no.uib.inf101.datastructure.Grid;

public class Main {

    public static void main(String[] args) {

        // Create a grid for colors
        Grid<Color> colorGrid = new Grid<>(3, 4, Color.WHITE); // Default to white if needed
        colorGrid.set(new CellPosition(0, 0), Color.RED);
        colorGrid.set(new CellPosition(0, 3), Color.BLUE);
        colorGrid.set(new CellPosition(2, 0), Color.YELLOW);
        colorGrid.set(new CellPosition(2, 3), Color.GREEN);

        // Create a grid for text
        Grid<String> textGrid = new Grid<>(8, 8, ""); // Default to empty string if needed
        textGrid.set(new CellPosition(0, 1), "Mandag");
        textGrid.set(new CellPosition(0, 2), "Tirsdag");
        // ... other textGrid.set calls ...

        // For a color grid view, uncomment this and comment out the text grid view
        // ColorGridView<Color> view = new ColorGridView<>(colorGrid);

        // For a text grid view
        // Use the generic version
		TextGridView<String> view = new TextGridView<>(textGrid);

        JFrame frame = new JFrame();
        frame.setContentPane(view);
        frame.setTitle("Grid View");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
