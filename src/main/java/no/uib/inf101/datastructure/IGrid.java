package no.uib.inf101.datastructure;

public interface IGrid<T> extends GridDimension, GridCellCollection<T> { 

  /**
   * Get the value of the cell at the given position.
   *
   * @param pos the position
   * @return the value of the cell of type T
   * @throws IndexOutOfBoundsException if the position is out of bounds
   */
  T get(CellPosition pos);

  /**
   * Set the value of the cell at the given position.
   *
   * @param pos the position
   * @param value the new value of type T
   * @throws IndexOutOfBoundsException if the position is out of bounds
   */
  void set(CellPosition pos, T value);

}
